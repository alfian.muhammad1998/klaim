<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_klaim extends CI_Model
{

  public function where($cookie)
  {
    $where = "WHERE a.is_deleted = 0 ";
    if (@$cookie['search']['bulan'] != '') {
      $where .= "AND MONTH(a.tgl_catat) = '" . $this->db->escape_like_str($cookie['search']['bulan']) . "' ";
    }
    if (@$cookie['search']['tahun'] != '') {
      $where .= "AND YEAR(a.tgl_catat) = '" . $this->db->escape_like_str($cookie['search']['tahun']) . "' ";
    }
    if (@$cookie['search']['user_id'] != '') {
      $where .= "AND a.user_id = '" . $this->db->escape_like_str($cookie['search']['user_id']) . "' ";
    }
    return $where;
  }

  public function list_data($cookie)
  {
    $where = $this->where($cookie);
    $sql = "SELECT
              a.*,
              MONTH ( a.tgl_catat ) AS bulan,
              YEAR ( a.tgl_catat ) AS tahun,
              b.user_fullname
            FROM
              dat_klaim a
              LEFT JOIN user b ON a.user_id = b.user_id
              $where
            ORDER BY "
      . $cookie['order']['field'] . " " . $cookie['order']['type'] .
      " LIMIT " . $cookie['cur_page'] . "," . $cookie['per_page'];
    $query = $this->db->query($sql);
    $result = $query->result_array();

    return $result;
  }

  public function all_data()
  {
    $where = "WHERE is_deleted = 0 ";

    $sql = "SELECT * FROM dat_klaim a $where ORDER BY tgl_catat";
    $query = $this->db->query($sql);
    return $query->result_array();
  }

  public function all_rows($cookie)
  {
    $where = $this->where($cookie);

    $sql = "SELECT
              COUNT( totals.total ) AS total 
            FROM
              (
              SELECT
                COUNT(*) AS total 
              FROM
                dat_klaim a
                LEFT JOIN user b ON a.user_id = b.user_id 
              $where 
              GROUP BY
                a.judul,
                a.user_id 
              ) AS totals";
    $query = $this->db->query($sql);
    return $query->row_array()['total'];
  }

  function by_field($field, $val)
  {
    $sql = "SELECT *, MONTH ( tgl_catat ) AS bulan, YEAR ( tgl_catat ) AS tahun FROM dat_klaim WHERE $field = ?";
    $query = $this->db->query($sql, array($val));
    $row = $query->row_array();
    return $row;
  }

  function get_data($klaim_id = '')
  {
    $sql = "SELECT
              a.*,
              MONTH ( a.tgl_catat ) AS bulan,
              YEAR ( a.tgl_catat ) AS tahun,
              b.user_fullname
            FROM
              dat_klaim a
              LEFT JOIN user b ON a.user_id = b.user_id 
            WHERE
              a.klaim_id = ? 
            ORDER BY
              a.tgl_catat DESC";
    $query = $this->db->query($sql, array($klaim_id));
    $row = $query->row_array();
    return $row;
  }

  function list_klaim($klaim_id = '', $user_id = '')
  {
    $sql = "SELECT
              a.* 
            FROM
              dat_detail_klaim a 
            WHERE
              a.klaim_id = ? 
              AND a.user_id = ? 
            ORDER BY a.created_at ASC";
    $query = $this->db->query($sql, array($klaim_id, $user_id));
    $result = $query->result_array();
    return $result;
  }

  function list_lembur_cetak($user_id = '', $bulan = '', $tahun = '')
  {
    $sql = "SELECT
              a.*,
              SUM( a.jml_jam ) AS jml_jam_lembur,
              DAYNAME( a.waktu_lembur_mulai ) AS hari,
              DATE( a.waktu_lembur_mulai ) AS tanggal,
              MONTH ( a.waktu_lembur_mulai ) AS bulan,
              YEAR ( a.waktu_lembur_mulai ) AS tahun  
            FROM
              dat_klaim a 
            WHERE
              a.user_id = ? 
              AND MONTH ( a.waktu_lembur_mulai ) = ? 
              AND YEAR ( a.waktu_lembur_mulai ) = ?
            GROUP BY DATE(a.waktu_lembur_mulai)
            ORDER BY a.waktu_lembur_mulai ASC";
    $query = $this->db->query($sql, array($user_id, $bulan, $tahun));
    $result = $query->result_array();
    // 
    foreach ($result as $key => $val) {
      $result[$key]['list_waktu'] = $this->list_waktu_lembur($user_id, $bulan, $tahun, $val['tanggal']);
    }
    return $result;
  }

  function list_waktu_lembur($user_id = '', $bulan = '', $tahun = '', $tanggal = '')
  {
    $sql = "SELECT
              a.waktu_lembur_mulai,
              a.waktu_lembur_selesai,
              TIME( a.waktu_lembur_mulai ) AS waktu_mulai,
              TIME( a.waktu_lembur_selesai ) AS waktu_selesai 
            FROM
              dat_klaim a 
            WHERE
              a.user_id = ? 
              AND MONTH ( a.waktu_lembur_mulai ) = ? 
              AND YEAR ( a.waktu_lembur_mulai ) = ? 
              AND DATE ( a.waktu_lembur_mulai ) = ?";
    $query = $this->db->query($sql, array($user_id, $bulan, $tahun, $tanggal));
    $result = $query->result_array();
    // 
    $list_waktu = '';
    $lastElement = end($result);
    foreach ($result as $key => $val) {
      if ($val == $lastElement) {
        $list_waktu .= $val['waktu_mulai'] . ' - ' . $val['waktu_selesai'];
      } else {
        $list_waktu .= $val['waktu_mulai'] . ' - ' . $val['waktu_selesai'] . ' & ';
      }
    }
    return $list_waktu;
  }

  public function list_pegawai()
  {
    $where = "WHERE role_id = '760fdaab-3074-42dc-9182-19cedb027fdf' ";
    if (@$this->session->userdata('role_id') == '760fdaab-3074-42dc-9182-19cedb027fdf') {
      $where .= "AND user_id = '" . @$this->session->userdata('user_id') . "' ";
    }
    $sql = "SELECT * FROM user $where ORDER BY user_fullname";
    $query = $this->db->query($sql);
    return $query->result_array();
  }

  public function save($id = null)
  {
    $data = html_escape($this->input->post());

    if ($id == null) {
      // insert dat_klaim
      $dat_klaim = array(
        'user_id' => @$data['user_id'],
        'tgl_catat' => to_date(@$data['tgl_catat'], '', 'full_date'),
        'judul' => @$data['judul'],
        'created_at' => date('Y-m-d H:i:s'),
        'created_by' => @$this->session->userdata('user_fullname'),
      );
      $this->db->insert('dat_klaim', $dat_klaim);
      $klaim_id = $this->db->insert_id();
      // end insert dat_klaim
    } else {
      // update dat_klaim
      $dat_klaim = array(
        'user_id' => @$data['user_id'],
        'tgl_catat' => to_date(@$data['tgl_catat'], '', 'full_date'),
        'judul' => @$data['judul'],
        'updated_at' => date('Y-m-d H:i:s'),
        'updated_by' => @$this->session->userdata('user_fullname'),
      );
      $this->db->where('klaim_id', $id)->update('dat_klaim', $dat_klaim);
      $klaim_id = @$id;
      // end update dat_klaim
    }

    return $klaim_id;
  }

  public function verifikasi($user_id = '', $bulan = '', $tahun = '')
  {
    $waktu_verifikasi = date('Y-m-d H:i:s');
    $sql = "UPDATE dat_klaim
            SET status_verifikasi = 1, waktu_verifikasi = '$waktu_verifikasi'
            WHERE user_id = '$user_id' AND MONTH(waktu_lembur_mulai) = $bulan AND YEAR(waktu_lembur_mulai) = $tahun";
    $this->db->query($sql);
  }

  public function batal_verifikasi($user_id = '', $bulan = '', $tahun = '')
  {
    $waktu_verifikasi = date('Y-m-d H:i:s');
    $sql = "UPDATE dat_klaim
            SET status_verifikasi = 0, waktu_verifikasi = NULL
            WHERE user_id = '$user_id' AND MONTH(waktu_lembur_mulai) = $bulan AND YEAR(waktu_lembur_mulai) = $tahun";
    $this->db->query($sql);
  }

  public function update($id, $data)
  {
    $data['updated_at'] = date('Y-m-d H:i:s');
    $data['updated_by'] = $this->session->userdata('fullname');
    $this->db->where('menu_id', $id)->update('menu', $data);
  }

  public function delete($klaim_id = null)
  {
    $sql = "DELETE FROM dat_klaim
            WHERE klaim_id = '$klaim_id'";
    $this->db->query($sql);
  }

  public function delete_lembur($klaim_id = '')
  {
    $this->db->where('klaim_id', $klaim_id)->delete('dat_klaim');
  }
}
