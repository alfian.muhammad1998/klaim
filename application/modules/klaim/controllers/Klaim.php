<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Klaim extends MY_Controller
{

  var $menu_id, $menu, $cookie;

  function __construct()
  {
    parent::__construct();

    $this->load->model(array(
      'config/m_config',
      'm_klaim',
      'user/m_user'
    ));

    $this->menu_id = '03';
    $this->menu = $this->m_config->get_menu($this->menu_id);
    if ($this->menu == null) redirect(site_url() . '/error/error_403');

    //cookie 
    $this->cookie = get_cookie_menu($this->menu_id);
    if (@$this->session->userdata('role_id') == '760fdaab-3074-42dc-9182-19cedb027fdf') {
      $user_id = @$this->session->userdata('user_id');
    } else {
      $user_id = '';
    }
    if ($this->cookie['search'] == null) $this->cookie['search'] = array('bulan' => '', 'tahun' => '', 'user_id' => @$user_id);
    if ($this->cookie['order'] == null) $this->cookie['order'] = array('field' => 'a.tgl_catat', 'type' => 'DESC');
    if ($this->cookie['per_page'] == null) $this->cookie['per_page'] = 10;
    if ($this->cookie['cur_page'] == null) 0;
  }

  public function index()
  {
    authorize($this->menu, '_read');
    //cookie
    $this->cookie['cur_page'] = $this->uri->segment(3, 0);
    $this->cookie['total_rows'] = $this->m_klaim->all_rows($this->cookie);
    set_cookie_menu($this->menu_id, $this->cookie);
    //main data
    $data['menu'] = $this->menu;
    $data['cookie'] = $this->cookie;
    $data['main'] = $this->m_klaim->list_data($this->cookie);
    $data['pagination_info'] = pagination_info(count($data['main']), $this->cookie);
    $data['list_pegawai'] = $this->m_klaim->list_pegawai();
    $data['list_bulan'] = list_bulan();
    $data['list_tahun'] = list_tahun();
    //set pagination
    set_pagination($this->menu, $this->cookie);
    //render
    $this->render('index', $data);
  }

  public function form($id = null)
  {
    ($id == null) ? authorize($this->menu, '_create') : authorize($this->menu, '_update');
    if ($id == null) {
      create_log(2, $this->menu['menu_name']);
      $data['main'] = null;
    } else {
      create_log(3, $this->menu['menu_name']);
      $data['main'] = $this->m_klaim->by_field('klaim_id', $id);
    }
    $data['id'] = $id;
    $data['menu'] = $this->menu;
    $data['all'] = $this->m_klaim->all_data();
    $data['list_pegawai'] = $this->m_klaim->list_pegawai();
    $this->render('form', $data);
  }

  public function form_klaim($id = null)
  {
    ($id == null) ? authorize($this->menu, '_create') : authorize($this->menu, '_update');
    if ($id == null) {
      create_log(2, $this->menu['menu_name']);
      $data['main'] = null;
    } else {
      create_log(3, $this->menu['menu_name']);
      $data['main'] = $this->m_klaim->by_field('klaim_id', $id);
    }
    $data['id'] = $id;
    $data['menu'] = $this->menu;
    $data['all'] = $this->m_klaim->all_data();
    $data['list_pegawai'] = $this->m_klaim->list_pegawai();
    $this->render('form_klaim', $data);
  }

  public function detail($klaim_id = null)
  {
    $data['klaim_id'] = $klaim_id;
    $data['menu'] = $this->menu;
    $data['main'] = $this->m_klaim->get_data($klaim_id);
    $data['list_klaim'] = $this->m_klaim->list_klaim($klaim_id, @$data['main']['user_id']);
    $this->render('detail', $data);
  }

  public function verifikasi($user_id = null, $bulan = null, $tahun = null)
  {
    $this->m_klaim->verifikasi($user_id, $bulan, $tahun);
    $this->session->set_flashdata('flash_success', 'Data berhasil disimpan.');
    redirect(site_url() . '/' . $this->menu['controller'] . '/' . $this->menu['url'] . '/' . $this->cookie['cur_page']);
  }

  public function batal_verifikasi($user_id = null, $bulan = null, $tahun = null)
  {
    $this->m_klaim->batal_verifikasi($user_id, $bulan, $tahun);
    $this->session->set_flashdata('flash_success', 'Data berhasil disimpan.');
    redirect(site_url() . '/' . $this->menu['controller'] . '/' . $this->menu['url'] . '/' . $this->cookie['cur_page']);
  }

  public function save($id = null)
  {
    ($id == null) ? authorize($this->menu, '_create') : authorize($this->menu, '_update');
    $this->m_klaim->save($id);
    $this->session->set_flashdata('flash_success', 'Data berhasil disimpan.');
    if (@$id != '') {
      redirect(site_url() . '/' . $this->menu['controller'] . '/index');
    } else {
      redirect(site_url() . '/' . $this->menu['controller'] . '/detail/' . @$id);
    }
  }

  public function delete($klaim_id = null)
  {
    authorize($this->menu, '_delete');
    $this->m_klaim->delete($klaim_id);
    create_log(4, $this->menu['menu_name']);
    $this->session->set_flashdata('flash_success', 'Data berhasil dihapus.');
    redirect(site_url() . '/' . $this->menu['controller'] . '/' . $this->menu['url'] . '/' . $this->cookie['cur_page']);
  }

  public function delete_lembur($klaim_id = '', $user_id = '', $bulan = '', $tahun = '')
  {
    $this->m_klaim->delete_lembur($klaim_id);
    $main = $this->m_klaim->get_data($user_id, $bulan, $tahun);
    $this->session->set_flashdata('flash_success', 'Data berhasil dihapus.');
    if (@$main['klaim_id'] != '') {
      redirect(site_url() . '/' . $this->menu['controller'] . '/detail/' . @$user_id . '/' . @$bulan . '/' . @$tahun);
    } else {
      redirect(site_url() . '/' . $this->menu['controller'] . '/' . $this->menu['url'] . '/' . $this->cookie['cur_page']);
    }
  }

  public function status($type = null, $id = null)
  {
    authorize($this->menu, '_update');
    if ($type == 'enable') {
      $this->m_klaim->update($id, array('is_active' => 1));
    } else {
      $this->m_klaim->update($id, array('is_active' => 0));
    }
    create_log(3, $this->this->menu['menu_name']);
    redirect(site_url() . '/' . $this->menu['controller'] . '/' . $this->menu['url'] . '/' . $this->cookie['cur_page']);
  }

  function cetak_detail($user_id = null, $bulan = null, $tahun = null)
  {
    ini_set('memory_limit', '-1');
    // Header
    $main = $this->m_klaim->list_lembur_cetak($user_id, $bulan, $tahun);
    $user = $this->m_user->by_field('user_id', $user_id);
    //
    //Configuration -------------------------------------------------------------------------
    $this->load->file(APPPATH . 'libraries/PHPExcel.php');
    $master_cetak = BASEPATH . 'master_cetak/lembur_detail.xlsx';
    $PHPExcel = PHPExcel_IOFactory::load($master_cetak);

    // sheet 1
    $PHPExcel->setActiveSheetIndex(0);

    //align center
    $align_center = array(
      'alignment' => array(
        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
      )
    );
    //align left
    $align_left = array(
      'alignment' => array(
        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
      )
    );
    //align right
    $align_right = array(
      'alignment' => array(
        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
      )
    );
    //font bold
    $font_bold = array(
      'font' => array(
        'bold'  => 'bold'
      )
    );
    //font underline
    $font_underline = array(
      'font' => array(
        'underline'  => 'underline'
      )
    );
    //font size 12
    $font_size_12 = array(
      'font' => array(
        'size'  => 12
      )
    );
    //wrap text
    $wrap_text = array(
      'alignment' => array(
        'wrap' => 'wrap',
      )
    );
    //create border
    $styleArray = array(
      'borders' => array(
        'allborders' => array(
          'style' => PHPExcel_Style_Border::BORDER_THIN,
          'color' => array('rgb' => '111111'),
        ),
      ),
      'font' => array(
        'size'  => 11,
        'name'  => 'Arial'
      ),
    );
    //Header
    $PHPExcel->getActiveSheet()->setCellValue("A1", strtoupper('DAFTAR LEMBUR PERIODE ' . month($bulan) . ' ' . $tahun));
    $PHPExcel->getActiveSheet()->setCellValue("A2", strtoupper(@$user['user_fullname']));

    // Body
    $i = 5;
    $no = 1;
    foreach ($main as $row) {
      $PHPExcel->getActiveSheet()->setCellValue("A$i", $no++);
      $PHPExcel->getActiveSheet()->setCellValue("B$i", to_date_indo($row['tanggal']));
      $PHPExcel->getActiveSheet()->setCellValue("E$i", date_dayname($row['hari']));
      $PHPExcel->getActiveSheet()->setCellValue("F$i", @$row['list_waktu']);
      $PHPExcel->getActiveSheet()->setCellValue("G$i", $row['jml_jam_klaim']);
      $PHPExcel->getActiveSheet()->setCellValue("H$i", $row['uraian']);
      //
      $i++;
    }

    // $PHPExcel->getActiveSheet()->setCellValue("A$i", "Total Akhir");
    // $PHPExcel->getActiveSheet()->getStyle("A$i")->applyFromArray($font_bold);
    // $PHPExcel->getActiveSheet()->mergeCells("A$i:B$i");

    // $PHPExcel->getActiveSheet()->setCellValue("C$i", $tot_akhir_total);
    // $PHPExcel->getActiveSheet()->getStyle("C$i")->applyFromArray($font_bold);

    // Creating Border
    $last_cell = "H" . ($i - 1);
    $PHPExcel->getActiveSheet()->getStyle("A4:$last_cell")->applyFromArray($styleArray);

    // Save it as file ------------------------------------------------------------------
    $file_export = 'LEMBUR - ' . strtoupper($user['user_fullname']) . ' - ' . strtoupper(month(@$bulan)) . ' ' . $tahun;
    //
    ob_end_clean();
    header('Content-Type: application/vnd.ms-excel2007');
    header('Content-Disposition: attachment; filename="' . $file_export . '.xlsx"');
    $objWriter = PHPExcel_IOFactory::createWriter($PHPExcel, 'Excel2007');
    $objWriter->save('php://output');
    //-----------------------------------------------------------------------------------
  }

  public function multiple($type = null)
  {
    $data = $this->input->post(null, true);
    if (isset($data['checkitem'])) {
      foreach ($data['checkitem'] as $key) {
        switch ($type) {
          case 'delete':
            authorize($this->menu, '_delete');
            $this->m_klaim->delete($key);
            $flash = 'Data berhasil dihapus.';
            $t = 4;
            break;

          case 'enable':
            authorize($this->menu, '_update');
            $this->m_klaim->update($key, array('is_active' => 1));
            $flash = 'Data berhasil diaktifkan.';
            $t = 3;
            break;

          case 'disable':
            authorize($this->menu, '_update');
            $this->m_klaim->update($key, array('is_active' => 0));
            $flash = 'Data berhasil dinonaktifkan.';
            $t = 3;
            break;
        }
      }
    }
    create_log($t, $this->menu['menu_name']);
    $this->session->set_flashdata('flash_success', $flash);
    redirect(site_url() . '/' . $this->menu['controller'] . '/' . $this->menu['url'] . '/' . $this->cookie['cur_page']);
  }

  public function ajax($type = null, $id = null)
  {
    if ($type == 'check_id') {
      $data = $this->input->post();
      $cek = $this->m_klaim->by_field('menu_id', $data['menu_id']);
      if ($id == null) {
        echo ($cek != null) ? 'false' : 'true';
      } else {
        echo ($id != $data['menu_id'] && $cek != null) ? 'false' : 'true';
      }
    }
  }
}
