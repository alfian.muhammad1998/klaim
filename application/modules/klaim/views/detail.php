<script>
  $(document).ready(function() {
    $("#form").validate({
      rules: {
        menu_id: {
          remote: {
            type: 'post',
            url: "<?= site_url() . '/' . $menu['controller'] . '/ajax/check_id/' . $id ?>",
            data: {
              'menu_id': function() {
                return $('#menu_id').val();
              }
            },
            dataType: 'json'
          }
        }
      },
      messages: {
        menu_id: {
          remote: "Kode sudah digunakan"
        }
      },
      errorElement: "em",
      errorPlacement: function(error, element) {
        error.addClass("invalid-feedback");
        if (element.prop("type") === "checkbox") {
          error.insertAfter(element.next("label"));
        } else if ($(element).hasClass('select2')) {
          error.insertAfter(element.next(".select2-container")).addClass('mt-1');
        } else {
          error.insertAfter(element);
        }
      },
      highlight: function(element, errorClass, validClass) {
        $(element).addClass("is-invalid").removeClass("is-valid");
      },
      unhighlight: function(element, errorClass, validClass) {
        $(element).addClass("is-valid").removeClass("is-invalid");
      },
      submitHandler: function(form) {
        $(".btn-submit").html('<i class="fas fa-spin fa-spinner"></i> Proses');
        $(".btn-submit").addClass('disabled');
        $(".btn-cancel").addClass('disabled');
        form.submit();
      }
    });
  });
</script>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-0">
        <div class="col-sm-6">
          <h5 class="m-0 text-dark"><i class="<?= @$menu['icon'] ?>"></i> <?= @$menu['menu_name'] ?></h5>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item active">Input Data</li>
            <li class="breadcrumb-item active"><?= @$menu['menu_name'] ?></li>
            <li class="breadcrumb-item active">Detail</li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div><!-- /.content-header -->
  <!-- Main content -->
  <div class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Detail <?= $menu['menu_name'] ?></h3>
            </div>
            <div class="card-body">
              <div class="flash-error" data-flasherror="<?= $this->session->flashdata('flash_error') ?>"></div>
              <div class="row">
                <div class="col-lg-6">
                  <div class="table-responsive">
                    <table class="table table-sm table-striped table-bordered">
                      <tbody>
                        <tr>
                          <td class="text-left" width="200">Nama Pegawai</td>
                          <td class="text-center" width="50">:</td>
                          <td><?= @$main['user_fullname'] ?></td>
                        </tr>
                        <tr>
                          <td class="text-left" width="200">Bulan - Tahun</td>
                          <td class="text-center" width="50">:</td>
                          <td><?= month(@$main['bulan']) ?> - <?= @$main['tahun'] ?></td>
                        </tr>
                        <tr>
                          <td class="text-left" width="200">Tanggal Catat</td>
                          <td class="text-center" width="50">:</td>
                          <td><?= to_date(@$main['tgl_catat'], '', 'full_date') ?></td>
                        </tr>
                        <tr>
                          <td class="text-left" width="200">Total Nominal Klaim</td>
                          <td class="text-center" width="50">:</td>
                          <td></td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
            <div class="card-header">
              <h3 class="card-title">Rincian Data Lembur</h3>
            </div>
            <div class="card-body">
              <a class="btn btn-sm btn-primary mb-3" href="<?= site_url() . '/' . $menu['controller'] . '/form_klaim' ?>"><i class="fas fa-plus-circle"></i> Tambah Klaim</a>
              <a class="btn btn-sm btn-success mb-3" href="<?= site_url() . '/' . $menu['controller'] . '/cetak_detail/' . @$user_id . '/' . @$bulan . '/' . @$tahun ?>" target="_blank"><i class="fas fa-print"></i> Cetak Detail Lembur</a>
              <div class="table-responsice">
                <table class="table table-bordered table-sm table-head-fixed">
                  <thead>
                    <tr>
                      <th class="text-center" width="30">No.</th>
                      <th class="text-center" width="40">Aksi</th>
                      <th class="text-center" width="400">Keterangan Klaim</th>
                      <th class="text-center" width="150">Nominal</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php if (count(@$list_klaim) == 0) : ?>
                      <tr>
                        <td class="text-center" colspan="99">Tidak ada data!</td>
                      </tr>
                    <?php else : ?>
                      <?php $no = 1;
                      foreach ($list_klaim as $row) : ?>
                        <tr>
                          <td class="text-center text-middle"><?= $no++ ?></td>
                          <td class="text-center text-middle">
                            <button type="button" class="btn btn-primary btn-xs dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                              Aksi
                            </button>
                            <div class="dropdown-menu">
                              <a class="dropdown-item text-primary" href="<?= site_url() . '/' . $menu['controller'] . '/form/' . $row['klaim_id'] . '/' . $row['detail_klaim_id'] ?>"><i class="fas fa-edit mr-2 text-primary" style="font-size: 12px;"></i> Ubah</a>
                              <a class="dropdown-item text-danger btn-delete" href="<?= site_url() . '/' . $menu['controller'] . '/delete_lembur/' . $row['klaim_id'] . '/' . $row['detail_klaim_id'] . '/' . $row['user_id'] . '/' . $row['bulan'] . '/' . $row['tahun'] ?>"><i class="fas fa-trash mr-2 text-danger" style="font-size: 12px;"></i> Hapus</a>
                            </div>
                          </td>
                          <td class="text-middle text-left"><?= $row['keterangan'] ?></td>
                          <td class="text-middle text-right">Rp <?= num_id($row['nominal']) ?></td>
                        </tr>
                      <?php endforeach; ?>
                    <?php endif; ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </div><!-- /.content -->
</div>
<!-- /.content-wrapper -->