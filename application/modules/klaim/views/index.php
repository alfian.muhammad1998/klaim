<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h5 class="m-0 text-dark"><i class="<?= @$menu['icon'] ?>"></i> <?= @$menu['menu_name'] ?></h5>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item active">Input Data</li>
            <li class="breadcrumb-item active"><?= @$menu['menu_name'] ?></li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div><!-- /.content-header -->

  <!-- Main content -->
  <div class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Daftar <?= $menu['menu_name'] ?></h3>
            </div>
            <div class="card-body">
              <form id="form-search" action="<?= site_url() . '/app/search/' . $menu['menu_id'] ?>" method="post" autocomplete="off">
                <div class="row">
                  <div class="col-md-4">
                    <?php if ($menu['_create'] == 1 || $menu['_update'] == 1) : ?>
                      <a class="btn btn-sm btn-primary" href="<?= site_url() . '/' . $menu['controller'] . '/form' ?>"><i class="fas fa-plus-circle"></i> Tambah Master Klaim</a>
                    <?php endif; ?>
                  </div>
                  <div class="col-md-2">
                    <select class="form-control form-control-sm select2" name="bulan" id="bulan" onchange="$('#form-search').submit()">
                      <option value="">- Semua Bulan -</option>
                      <?php foreach ($list_bulan as $key => $val) : ?>
                        <option <?= (@$cookie['search']['bulan'] == @$key) ? 'selected' : '' ?> value="<?= @$key ?>"><?= @$val ?></option>
                      <?php endforeach; ?>
                    </select>
                  </div>
                  <div class="col-md-2">
                    <select class="form-control form-control-sm select2" name="tahun" id="tahun" onchange="$('#form-search').submit()">
                      <option value="">- Semua Tahun -</option>
                      <?php foreach ($list_tahun as $key => $val) : ?>
                        <option <?= (@$cookie['search']['tahun'] == @$key) ? 'selected' : '' ?> value="<?= @$key ?>"><?= @$val ?></option>
                      <?php endforeach; ?>
                    </select>
                  </div>
                  <div class="col-md-4">
                    <div class="row">
                      <div class="col-lg-10">
                        <select class="form-control form-control-sm <?= (@$this->session->userdata('role_id') != '760fdaab-3074-42dc-9182-19cedb027fdf') ? 'select2' : 'select2-hidden' ?>" name="user_id" id="user_id" onchange="$('#form-search').submit()">
                          <?php if (@$this->session->userdata('role_id') != '760fdaab-3074-42dc-9182-19cedb027fdf') : ?>
                            <option value="">- Semua Pegawai -</option>
                          <?php endif; ?>
                          <?php foreach ($list_pegawai as $pegawai) : ?>
                            <option <?= (@$cookie['search']['user_id'] == @$pegawai['user_id']) ? 'selected' : '' ?> value="<?= @$pegawai['user_id'] ?>"><?= @$pegawai['user_fullname'] ?></option>
                          <?php endforeach; ?>
                        </select>
                      </div>
                      <div class="col-lg-2">
                        <a class="btn btn-default w-100" href="<?= site_url() . '/app/reset/' . $menu['menu_id'] ?>"><i class="fas fa-sync-alt"></i></a>
                      </div>
                    </div>
                  </div>
                </div><!-- /.row -->
              </form>
              <div class="row mb-2 mt-2">
                <div class="col-md-6">
                  <div class="input-group-prepend">
                    <span class="mr-1 pt-1">Tampilkan </span>
                    <button type="button" class="btn btn-xs btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                      <?= @$cookie['per_page'] ?>
                    </button>
                    <span class="ml-1 pt-1">data.</span>
                    <div class="dropdown-menu">
                      <a class="dropdown-item <?= (@$cookie['per_page'] == 10) ? 'active' : '' ?>" href="<?= site_url() . '/app/per_page/' . $menu['menu_id'] . '/10' ?>">10</a>
                      <a class="dropdown-item <?= (@$cookie['per_page'] == 25) ? 'active' : '' ?>" href="<?= site_url() . '/app/per_page/' . $menu['menu_id'] . '/25' ?>">25</a>
                      <a class="dropdown-item <?= (@$cookie['per_page'] == 50) ? 'active' : '' ?>" href="<?= site_url() . '/app/per_page/' . $menu['menu_id'] . '/50' ?>">50</a>
                      <a class="dropdown-item <?= (@$cookie['per_page'] == 100) ? 'active' : '' ?>" href="<?= site_url() . '/app/per_page/' . $menu['menu_id'] . '/100' ?>">100</a>
                    </div>
                  </div>
                </div>
                <div class="col-md-6 text-right">
                  <span class="pt-1"><?= @$pagination_info ?></span>
                </div>
              </div><!-- /.row -->
              <div class="row">
                <div class="col-md-12">
                  <div class="flash-success" data-flashsuccess="<?= $this->session->flashdata('flash_success') ?>"></div>
                  <div class="table-responsive">
                    <table class="table table-bordered table-sm table-head-fixed">
                      <thead>
                        <tr>
                          <th class="text-center text-middle" width="20">No.</th>
                          <th class="text-center text-middle" width="30">Aksi</th>
                          <th class="text-center text-middle" width="150">Pegawai</th>
                          <th class="text-center text-middle" width="100">Bulan - Tahun</th>
                          <th class="text-center text-middle" width="300">Judul</th>
                        </tr>
                      </thead>
                      <?php if (@$main == null) : ?>
                        <tbody>
                          <tr>
                            <td class="text-center" colspan="99"><i>Tidak ada data!</i></td>
                          </tr>
                        </tbody>
                      <?php else : ?>
                        <tbody>
                          <form id="form-multiple" action="" method="post">
                            <?php $i = 1;
                            foreach ($main as $r) : ?>
                              <tr>
                                <td class="text-center text-middle"><?= $cookie['cur_page'] + ($i++) ?></td>
                                <td class="text-center text-middle">
                                  <button type="button" class="btn btn-primary btn-xs dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                    Aksi
                                  </button>
                                  <div class="dropdown-menu">
                                    <a class="dropdown-item text-primary" href="<?= site_url() . '/' . $menu['controller'] . '/detail/' . $r['klaim_id'] ?>"><i class="fas fa-bars mr-2 text-primary" style="font-size: 12px;"></i> Detail</a>
                                    <a class="dropdown-item text-primary" href="<?= site_url() . '/' . $menu['controller'] . '/form/' . $r['klaim_id'] ?>"><i class="fas fa-edit mr-2 text-primary" style="font-size: 12px;"></i> Ubah</a>
                                    <a class="dropdown-item text-danger btn-delete" href="<?= site_url() . '/' . $menu['controller'] . '/delete/' . $r['klaim_id'] ?>"><i class="fas fa-trash mr-2 text-danger" style="font-size: 12px;"></i> Hapus</a>
                                  </div>
                                </td>
                                <td class="text-middle"><?= $r['user_fullname'] ?></td>
                                <td class="text-center text-middle"><?= month($r['bulan']) ?> - <?= $r['tahun'] ?></td>
                                <td class="text-middle"><?= $r['judul'] ?></td>
                              </tr>
                            <?php endforeach; ?>
                          </form>
                        </tbody>
                      <?php endif; ?>
                    </table>
                  </div>
                </div>
              </div><!-- /.row -->
            </div>
            <div class="card-footer">
              <div class="row">
                <div class="col-md-6">
                </div>
                <div class="col-md-6 float-right">
                  <?php echo $this->pagination->create_links(); ?>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </div><!-- /.content -->
</div>
<!-- /.content-wrapper -->