<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_dashboard extends CI_Model
{

  function by_field($field, $val)
  {
    $sql = "SELECT * FROM user WHERE $field = ?";
    $query = $this->db->query($sql, array($val));
    $row = $query->row_array();
    return $row;
  }

  function get_data($user_id = '')
  {
    $sql = "SELECT * FROM user WHERE user_id = ?";
    $query = $this->db->query($sql, array($user_id));
    $row = $query->row_array();
    return $row;
  }

  public function pengaturan_user_save($data, $id = null)
  {
    if ($id == null) {
      $data['created_at'] = date('Y-m-d H:i:s');
      $data['created_by'] = $this->session->userdata('user_fullname');
      $this->db->insert('user', $data);
    } else {
      $data['updated_at'] = date('Y-m-d H:i:s');
      $data['updated_by'] = $this->session->userdata('user_fullname');
      $this->db->where('user_id', $id)->update('user', $data);
    }
  }
}
