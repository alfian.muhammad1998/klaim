<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0 text-dark"><?= @$menu['menu'] ?></h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item active"><?= @$menu['menu'] ?></li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div><!-- /.content-header -->

  <!-- Main content -->
  <div class="content">
    <div class="container-fluid">
      <div class="flash-success" data-flashsuccess="<?= $this->session->flashdata('flash_success') ?>"></div>
      <div class="row mb-4 mt-1">
        <div class="col-lg-12">
          <h4 class="m-0 text-center">Selamat Datang di Aplikasi Lembur Pegawai</h4>
          <h4 class="m-0 text-center">CV. ADD Komputer</h4>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-3 col-6">
          <!-- small box -->
          <div class="small-box bg-info">
            <div class="inner">
              <!-- <h3>150</h3> -->
              <h5 class="mb-4">Coming Soon</h5>

              <p>Total Hari Lembur <br> Desember - 2021</p>
            </div>
            <div class="icon">
              <i class="far fa-calendar-alt"></i>
            </div>
            <a class="small-box-footer"></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-6">
          <!-- small box -->
          <div class="small-box bg-success">
            <div class="inner">
              <!-- <h3>53<sup style="font-size: 20px">%</sup></h3> -->
              <h5 class="mb-4">Coming Soon</h5>

              <p>Total Jam Lembur <br> Desember - 2021</p>
            </div>
            <div class="icon">
              <i class="far fa-clock"></i>
            </div>
            <a class="small-box-footer"></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-6">
          <!-- small box -->
          <div class="small-box bg-warning">
            <div class="inner">
              <!-- <h3>0</h3> -->
              <h5 class="mb-4">Coming Soon</h5>

              <p>Lorem Ipsum <br> Dolor Sit Amet</p>
            </div>
            <div class="icon">
              <i class="ion ion-person-add"></i>
            </div>
            <a class="small-box-footer"></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-6">
          <!-- small box -->
          <div class="small-box bg-danger">
            <div class="inner">
              <!-- <h3>0</h3> -->
              <h5 class="mb-4">Coming Soon</h5>

              <p>Lorem Ipsum <br> Dolor Sit Amet</p>
            </div>
            <div class="icon">
              <i class="ion ion-pie-graph"></i>
            </div>
            <a class="small-box-footer"></a>
          </div>
        </div>
        <!-- ./col -->
      </div>
      <div class="row">
        <div class="col-lg-6">
          <div class="card card-outline card-primary">
            <div class="card-header">
              <h3 class="card-title"><i class="fas fa-user-clock"></i> Rincian Lembur Per Bulan</h3>
              <!-- /.card-tools -->
            </div>
            <!-- /.card-header -->
            <div class="card-body" style="display: block;">
              <!-- <div class="table-responsive">
                <table class="table table-bordered table-sm table-head-fixed">
                  <thead>
                    <tr>
                      <th class="text-center text-middle" width="40">No.</th>
                      <th class="text-center text-middle">Bulan - Tahun</th>
                      <th class="text-center text-middle">Jml. Hari Lembur</th>
                      <th class="text-center text-middle">Jml. Jam Lembur</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td class="text-center" colspan="99"><i>Tidak ada data!</i></td>
                    </tr>
                  </tbody>
                </table>
              </div> -->
              <div class="text-center">Coming Soon</div>
            </div>
            <!-- /.card-body -->
          </div>
        </div>
        <div class="col-lg-6">
          <div class="card card-outline card-primary">
            <div class="card-header">
              <h3 class="card-title"><i class="fas fa-money-bill-wave"></i> Rincian Klaim</h3>
              <!-- /.card-tools -->
            </div>
            <!-- /.card-header -->
            <div class="card-body" style="display: block;">
              <div class="text-center">Cooming Soon</div>
            </div>
            <!-- /.card-body -->
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-12">
          <div class="card card-outline card-primary">
            <div class="card-header">
              <h3 class="card-title"><i class="fas fa-chart-line"></i> Chart Line Lembur Per Tahun 2021</h3>
              <!-- /.card-tools -->
            </div>
            <!-- /.card-header -->
            <div class="card-body" style="display: block;">
              <!-- <div id="container" style="width: 100%;">
                <canvas id="canvas"></canvas>
              </div> -->
              <div class="text-center">Coming Soon</div>
            </div>
            <!-- /.card-body -->
          </div>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </div><!-- /.content -->
</div>
<!-- /.content-wrapper -->

<script>
  var xValues = [50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150];
  var yValues = [7, 8, 8, 9, 9, 9, 10, 11, 14, 14, 15];
  var ctx = document.getElementById('canvas').getContext('2d');
  window.myBar = new Chart(ctx, {
    type: 'line',
    data: {
      labels: xValues,
      datasets: [{
        fill: false,
        lineTension: 0,
        backgroundColor: "rgba(0,0,255,1.0)",
        borderColor: "rgba(0,0,255,0.1)",
        data: yValues
      }]
    },
    options: {
      legend: {
        display: false
      },
      scales: {
        yAxes: [{
          ticks: {
            min: 6,
            max: 16
          }
        }],
      }
    }
  });
</script>