<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Dashboard extends MY_Controller
{

  var $menu_id, $menu;

  function __construct()
  {
    parent::__construct();

    $this->load->model(array(
      'config/m_config',
      'm_dashboard'
    ));

    $this->menu_id = '01';
    $this->menu = $this->m_config->get_menu($this->menu_id);
    if ($this->menu == null) redirect(site_url() . '/error/error_403');
  }

  public function index()
  {
    $data['menu'] = $this->menu;
    $this->render('index', $data);
  }

  public function pengaturan_user($user_id = '')
  {
    $data['menu'] = $this->menu;
    $data['main'] = $this->m_dashboard->get_data($user_id);
    $data['user_id'] = $user_id;
    $this->render('pengaturan_user', $data);
  }

  public function pengaturan_user_save($id = null)
  {
    $data = html_escape($data = $this->input->post(null, true));
    if (!isset($data['is_active'])) {
      $data['is_active'] = 0;
    }
    $cek = $this->m_dashboard->by_field('user_name', $data['user_name']);
    if ($id == null) {
      if ($cek != null) {
        $this->session->set_flashdata('flash_error', 'Nama pengguna sudah ada di sistem.');
        redirect(site_url() . '/' .  $this->menu['controller']  . '/form/');
      }
      $data['user_id'] = $this->uuid->v4();
      $data['hash'] = password_hash($data['password'], PASSWORD_BCRYPT, ['cost' => 12]);
      if (!empty($_FILES["photo"]["name"])) {
        $data['photo'] = $this->_uploadImage();
      } else {
        $data['photo'] = '';
      }
      unset($data['password'], $data['password_confirm'], $data['old_photo']);
      $this->m_dashboard->pengaturan_user_save($data, $id);
      create_log(2, $this->menu['menu_name']);
      $this->session->set_flashdata('flash_success', 'Data berhasil ditambahkan.');
    } else {
      if ($data['old'] != $data['user_name'] && $cek != null) {
        $this->session->set_flashdata('flash_error', 'Nama pengguna sudah ada di sistem.');
        redirect(site_url() . '/' . $this->menu['controller'] . '/form/' . $id);
      }
      if (!empty($_FILES["photo"]["name"])) {
        $data['photo'] = $this->_uploadImage();
      } else {
        $data['photo'] = $data['old_photo'];
      }
      unset($data['old'], $data['old_photo']);
      $this->m_dashboard->pengaturan_user_save($data, $id);
      create_log(3, $this->menu['menu_name']);
      $this->session->set_flashdata('flash_success', 'Data berhasil diubah. Silahkan logout untuk melihat hasil');
    }
    redirect(site_url() . '/' . $this->menu['controller'] . '/' . $this->menu['url']);
  }

  public function save_password($id = null)
  {
    $data = html_escape($this->input->post());
    $data['hash'] = password_hash($data['password'], PASSWORD_BCRYPT, ['cost' => 12]);
    unset($data['password'], $data['password_confirm'], $data['old_photo']);
    $this->m_dashboard->pengaturan_user_save($data, $id);
    $this->session->set_flashdata('flash_success', 'Data berhasil diubah.');
    redirect(site_url() . '/' . $this->menu['controller'] . '/' . $this->menu['url']);
  }

  private function _uploadImage()
  {
    $config['upload_path']          = FCPATH . '/images/users/';
    $config['allowed_types']        = 'gif|jpg|png|jpeg|bmp|svg';
    $config['encrypt_name']         = true;
    $config['overwrite']            = true;
    $config['max_size']             = 2048;

    $this->load->library('upload', $config);

    if ($this->upload->do_upload('photo')) {
      return $this->upload->data("file_name");
    } else {
      var_dump($this->upload->display_errors());
      die();
    }

    return "";
  }

  public function ajax($type = null, $id = null)
  {
    if ($type == 'check_old') {
      $data = $this->input->post();
      $cek = $this->m_dashboard->by_field('user_name', $data['user_name']);
      $old = $this->m_dashboard->by_field('user_id', $id);
      if ($id == null) {
        echo ($cek != null) ? 'false' : 'true';
      } else {
        echo ($cek != null) ? (($cek['user_name'] == $old['user_name']) ? 'true' : 'false') : 'true';
      }
    }
  }
}
