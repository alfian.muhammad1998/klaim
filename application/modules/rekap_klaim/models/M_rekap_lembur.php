<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_rekap_lembur extends CI_Model
{

  public function where($cookie)
  {
    $where = "WHERE a.is_deleted = 0 ";
    if (@$cookie['search']['bulan'] != '') {
      $where .= "AND MONTH(a.waktu_lembur_mulai) = '" . $this->db->escape_like_str($cookie['search']['bulan']) . "' ";
    }
    if (@$cookie['search']['tahun'] != '') {
      $where .= "AND YEAR(a.waktu_lembur_mulai) = '" . $this->db->escape_like_str($cookie['search']['tahun']) . "' ";
    }
    if (@$cookie['search']['user_id'] != '') {
      $where .= "AND a.user_id = '" . $this->db->escape_like_str($cookie['search']['user_id']) . "' ";
    }
    return $where;
  }

  public function list_data($cookie)
  {
    $where = $this->where($cookie);
    $sql = "SELECT
              a.user_id,
              DAY ( a.waktu_lembur_mulai ) AS tanggal,
              MONTH ( a.waktu_lembur_mulai ) AS bulan,
              YEAR ( a.waktu_lembur_mulai ) AS tahun,
              b.user_fullname,
              count(distinct date(a.waktu_lembur_mulai)) AS jml_hari_lembur, 
              -- SUM( a.jml_jam ) AS jml_jam_lembur,
              a.waktu_entri,
              a.status_verifikasi,
              a.waktu_verifikasi
            FROM
              dat_lembur a
              LEFT JOIN user b ON a.user_id = b.user_id
              $where
            GROUP BY
              YEAR ( a.waktu_lembur_mulai ),
              MONTH ( a.waktu_lembur_mulai ),
              a.user_id 
            ORDER BY a.waktu_lembur_mulai DESC";
    $query = $this->db->query($sql);
    $result = $query->result_array();

    foreach ($result as $key => $val) {
      $result[$key]['jml_jam_lembur'] = $this->count_jam_lembur($val['user_id'], $val['bulan'], $val['tahun']);
    }
    return $result;
  }

  public function list_cetak_data($bulan = null, $tahun = null, $user_id = null)
  {
    $where = "";
    if (@$user_id != '') {
      $where .= "AND a.user_id = '" . @$user_id . "' ";
    }
    $sql = "SELECT
              a.user_id,
              DAY ( a.waktu_lembur_mulai ) AS tanggal,
              MONTH ( a.waktu_lembur_mulai ) AS bulan,
              YEAR ( a.waktu_lembur_mulai ) AS tahun,
              b.user_fullname,
              count(distinct date(a.waktu_lembur_mulai)) AS jml_hari_lembur, 
              -- SUM( a.jml_jam ) AS jml_jam_lembur,
              a.waktu_entri,
              a.status_verifikasi,
              a.waktu_verifikasi
            FROM
              dat_lembur a
              LEFT JOIN user b ON a.user_id = b.user_id
            WHERE a.is_deleted = 0
              AND MONTH(a.waktu_lembur_mulai) = '" . @$bulan . "' 
              AND YEAR(a.waktu_lembur_mulai) = '" . @$tahun . "' 
              $where 
            GROUP BY
              YEAR ( a.waktu_lembur_mulai ),
              MONTH ( a.waktu_lembur_mulai ),
              a.user_id 
            ORDER BY a.waktu_lembur_mulai DESC";
    $query = $this->db->query($sql);
    $result = $query->result_array();

    foreach ($result as $key => $val) {
      $result[$key]['jml_jam_lembur'] = $this->count_jam_lembur($val['user_id'], $val['bulan'], $val['tahun']);
    }
    return $result;
  }

  function count_jam_lembur($user_id = '', $bulan = '', $tahun = '')
  {
    $sql = "SELECT
              a.jml_jam
            FROM
              dat_lembur a
              LEFT JOIN user b ON a.user_id = b.user_id 
            WHERE
              a.user_id = ? 
              AND MONTH ( a.waktu_lembur_mulai ) = ? 
              AND YEAR ( a.waktu_lembur_mulai ) = ? 
            ORDER BY
              a.waktu_lembur_mulai DESC";
    $query = $this->db->query($sql, array($user_id, $bulan, $tahun));
    $result = $query->result_array();

    $time = 0;
    foreach ($result as $key => $val) {
      $jml_jam = titik_to_titikdua($val['jml_jam']);
      $time += explode_time($jml_jam);
    }
    return second_to_hhmm($time);
  }

  public function all_data()
  {
    $where = "WHERE is_deleted = 0 ";

    $sql = "SELECT * FROM dat_lembur a $where ORDER BY waktu_entri";
    $query = $this->db->query($sql);
    return $query->result_array();
  }

  public function all_rows($cookie)
  {
    $where = $this->where($cookie);

    $sql = "SELECT COUNT(1) as total FROM dat_lembur a $where";
    $query = $this->db->query($sql);
    return $query->row_array()['total'];
  }

  function by_field($field, $val)
  {
    $sql = "SELECT *, MONTH ( waktu_lembur_mulai ) AS bulan, YEAR ( waktu_lembur_mulai ) AS tahun FROM dat_lembur WHERE $field = ?";
    $query = $this->db->query($sql, array($val));
    $row = $query->row_array();
    return $row;
  }

  function get_data($user_id = '', $bulan = '', $tahun = '')
  {
    $sql = "SELECT
              a.lembur_id,
              a.user_id,
              DAY ( a.waktu_lembur_mulai ) AS tanggal,
              MONTH ( a.waktu_lembur_mulai ) AS bulan,
              YEAR ( a.waktu_lembur_mulai ) AS tahun,
              b.user_fullname,
              count(
              DISTINCT date( a.waktu_lembur_mulai )) AS jml_hari_lembur,
              SUM( a.jml_jam ) AS jml_jam_lembur,
              a.waktu_entri,
              a.status_verifikasi,
              a.waktu_verifikasi 
            FROM
              dat_lembur a
              LEFT JOIN user b ON a.user_id = b.user_id 
            WHERE
              a.user_id = ? 
              AND MONTH ( a.waktu_lembur_mulai ) = ? 
              AND YEAR ( a.waktu_lembur_mulai ) = ? 
            GROUP BY
              YEAR ( a.waktu_lembur_mulai ),
              MONTH ( a.waktu_lembur_mulai ),
              a.user_id 
            ORDER BY
              a.waktu_lembur_mulai DESC";
    $query = $this->db->query($sql, array($user_id, $bulan, $tahun));
    $row = $query->row_array();
    return $row;
  }

  public function get_user($user_id = '')
  {
    $sql = "SELECT * FROM user WHERE user_id = ?";
    $query = $this->db->query($sql, $user_id);
    return $query->row_array();
  }

  function list_lembur($user_id = '', $bulan = '', $tahun = '')
  {
    $sql = "SELECT
              a.*, DAYNAME(a.waktu_lembur_mulai) AS hari, MONTH(a.waktu_lembur_mulai) AS bulan, YEAR(a.waktu_lembur_mulai) AS tahun 
            FROM
              dat_lembur a 
            WHERE
              a.user_id = ? 
              AND MONTH ( a.waktu_lembur_mulai ) = ? 
              AND YEAR ( a.waktu_lembur_mulai ) = ?";
    $query = $this->db->query($sql, array($user_id, $bulan, $tahun));
    $result = $query->result_array();
    return $result;
  }

  public function list_pegawai()
  {
    $where = "WHERE role_id = '760fdaab-3074-42dc-9182-19cedb027fdf' ";
    if (@$this->session->userdata('role_id') == '760fdaab-3074-42dc-9182-19cedb027fdf') {
      $where .= "AND user_id = '" . @$this->session->userdata('user_id') . "' ";
    }
    $sql = "SELECT * FROM user $where ORDER BY user_fullname";
    $query = $this->db->query($sql);
    return $query->result_array();
  }

  public function save($id = null)
  {
    $data = html_escape($this->input->post());

    if ($id == null) {
      // insert dat_lembur
      $dat_lembur = array(
        'user_id' => @$data['user_id'],
        'waktu_lembur_mulai' => to_date(@$data['waktu_lembur_mulai'], '', 'full_date'),
        'waktu_lembur_selesai' => to_date(@$data['waktu_lembur_selesai'], '', 'full_date'),
        'jml_jam' => @$data['jml_jam'],
        'uraian' => @$data['uraian'],
        'waktu_entri' => date('Y-m-d H:i:s'),
        'created_at' => date('Y-m-d H:i:s'),
        'created_by' => @$this->session->userdata('user_fullname'),
      );
      $this->db->insert('dat_lembur', $dat_lembur);
      $lembur_id = $this->db->insert_id();
      // end insert dat_lembur
    } else {
      // update dat_lembur
      $dat_lembur = array(
        'user_id' => @$data['user_id'],
        'waktu_lembur_mulai' => to_date(@$data['waktu_lembur_mulai'], '', 'full_date'),
        'waktu_lembur_selesai' => to_date(@$data['waktu_lembur_selesai'], '', 'full_date'),
        'jml_jam' => @$data['jml_jam'],
        'uraian' => @$data['uraian'],
        'waktu_entri' => date('Y-m-d H:i:s'),
        'updated_at' => date('Y-m-d H:i:s'),
        'updated_by' => @$this->session->userdata('user_fullname'),
      );
      $this->db->where('lembur_id', $id)->update('dat_lembur', $dat_lembur);
      $lembur_id = @$id;
      // end update dat_lembur
    }

    return $lembur_id;
  }

  public function verifikasi($user_id = '', $bulan = '', $tahun = '')
  {
    $waktu_verifikasi = date('Y-m-d H:i:s');
    $sql = "UPDATE dat_lembur
            SET status_verifikasi = 1, waktu_verifikasi = '$waktu_verifikasi'
            WHERE user_id = '$user_id' AND MONTH(waktu_lembur_mulai) = $bulan AND YEAR(waktu_lembur_mulai) = $tahun";
    $this->db->query($sql);
  }

  public function batal_verifikasi($user_id = '', $bulan = '', $tahun = '')
  {
    $waktu_verifikasi = date('Y-m-d H:i:s');
    $sql = "UPDATE dat_lembur
            SET status_verifikasi = 0, waktu_verifikasi = NULL
            WHERE user_id = '$user_id' AND MONTH(waktu_lembur_mulai) = $bulan AND YEAR(waktu_lembur_mulai) = $tahun";
    $this->db->query($sql);
  }

  public function update($id, $data)
  {
    $data['updated_at'] = date('Y-m-d H:i:s');
    $data['updated_by'] = $this->session->userdata('fullname');
    $this->db->where('menu_id', $id)->update('menu', $data);
  }

  public function delete($user_id = null, $bulan = null, $tahun = null)
  {
    $sql = "DELETE FROM dat_lembur
            WHERE user_id = '$user_id' AND MONTH(waktu_lembur_mulai) = $bulan AND YEAR(waktu_lembur_mulai) = $tahun";
    $this->db->query($sql);
  }

  public function delete_lembur($lembur_id = '')
  {
    $this->db->where('lembur_id', $lembur_id)->delete('dat_lembur');
  }
}
