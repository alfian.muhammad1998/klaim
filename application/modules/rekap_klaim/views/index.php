<script>
  $(document).ready(function() {
    $("#form-search").validate({
      rules: {},
      messages: {},
      errorElement: "em",
      errorPlacement: function(error, element) {
        error.addClass("invalid-feedback");
        if (element.prop("type") === "checkbox") {
          error.insertAfter(element.next("label"));
        } else if ($(element).hasClass('select2')) {
          error.insertAfter(element.next(".select2-container")).addClass('mt-1');
        } else {
          error.insertAfter(element);
        }
      },
      highlight: function(element, errorClass, validClass) {
        $(element).addClass("is-invalid").removeClass("is-valid");
      },
      unhighlight: function(element, errorClass, validClass) {
        $(element).addClass("is-valid").removeClass("is-invalid");
      },
      submitHandler: function(form) {
        $(".btn-submit").html('<i class="fas fa-spin fa-spinner"></i> Proses');
        $(".btn-submit").addClass('disabled');
        $(".btn-cancel").addClass('disabled');
        form.submit();
      }
    });
  });
</script>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h5 class="m-0 text-dark"><i class="<?= @$menu['icon'] ?>"></i> <?= @$menu['menu_name'] ?></h5>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item active">Rekapitulasi</li>
            <li class="breadcrumb-item active"><?= @$menu['menu_name'] ?></li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div><!-- /.content-header -->

  <!-- Main content -->
  <div class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Daftar <?= $menu['menu_name'] ?></h3>
            </div>
            <form id="form-search" action="<?= site_url() . '/app/search/' . $menu['menu_id'] ?>" method="post" autocomplete="off">
              <div class="card-body">
                <div class="row">
                  <div class="col-lg-6">
                    <div class="form-group row">
                      <label for="type" class="col-sm-2 col-form-label text-right">Bulan</label>
                      <div class="col-sm-4">
                        <select class="form-control form-control-sm select2" name="bulan" id="bulan" required>
                          <option value="">- Pilih Bulan -</option>
                          <?php foreach ($list_bulan as $key => $val) : ?>
                            <option <?= (@$cookie['search']['bulan'] == @$key) ? 'selected' : '' ?> value="<?= @$key ?>"><?= @$val ?></option>
                          <?php endforeach; ?>
                        </select>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="type" class="col-sm-2 col-form-label text-right">Tahun</label>
                      <div class="col-sm-4">
                        <select class="form-control form-control-sm select2" name="tahun" id="tahun" required>
                          <option value="">- Pilih Tahun -</option>
                          <?php foreach ($list_tahun as $key => $val) : ?>
                            <option <?= (@$cookie['search']['tahun'] == @$key) ? 'selected' : '' ?> value="<?= @$key ?>"><?= @$val ?></option>
                          <?php endforeach; ?>
                        </select>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="type" class="col-sm-2 col-form-label text-right">Pegawai</label>
                      <div class="col-sm-7">
                        <select class="form-control form-control-sm <?= (@$this->session->userdata('role_id') != '760fdaab-3074-42dc-9182-19cedb027fdf') ? 'select2' : 'select2-hidden' ?>" name="user_id" id="user_id">
                          <?php if (@$this->session->userdata('role_id') != '760fdaab-3074-42dc-9182-19cedb027fdf') : ?>
                            <option value="">- Semua Pegawai -</option>
                          <?php endif; ?>
                          <?php foreach ($list_pegawai as $pegawai) : ?>
                            <option <?= (@$cookie['search']['user_id'] == @$pegawai['user_id']) ? 'selected' : '' ?> value="<?= @$pegawai['user_id'] ?>"><?= @$pegawai['user_fullname'] ?></option>
                          <?php endforeach; ?>
                        </select>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="row mt-3">
                  <div class="col-md-11 offset-md-1">
                    <button type="submit" class="btn btn-sm btn-primary btn-submit"><i class="fas fa-search"></i> Tampilkan</button>
                    <?php if (@$cookie['search']['bulan'] != '') : ?>
                      <a href="<?= site_url() . '/app/reset/' . $menu['menu_id'] ?>" class="btn btn-sm btn-secondary btn-cancel"><i class="fas fa-times-circle"></i> Batal</a>
                      <a href="<?= site_url() . '/' . $menu['controller'] . '/cetak_excel/' . @$cookie['search']['bulan'] . '/' . @$cookie['search']['tahun'] . '/' . @$cookie['search']['user_id'] ?>" class="btn btn-sm btn-success"><i class="far fa-file-excel"></i> Cetak Rekap Excel</a>
                    <?php endif; ?>
                  </div>
                </div>
                <?php if (@$cookie['search']['bulan'] != '') : ?>
                  <div class="row mt-4">
                    <div class="col-lg-12">
                      <div class="table-responsive">
                        <table class="table table-bordered table-sm table-head-fixed">
                          <thead>
                            <tr>
                              <th class="text-center text-middle" width="40" rowspan="2">No.</th>
                              <th class="text-center text-middle" width="200" rowspan="2">Pegawai</th>
                              <th class="text-center text-middle" width="100" rowspan="2">Bulan - Tahun</th>
                              <th class="text-center text-middle" width="300" colspan="3">Lembur Pegawai</th>
                              <th class="text-center text-middle" width="150" colspan="2">Verifikasi</th>
                            </tr>
                            <tr>
                              <th class="text-center text-middle" width="100">Jml. Hari Lembur</th>
                              <th class="text-center text-middle" width="100">Jml. Jam Lembur</th>
                              <th class="text-center text-middle" width="100">Waktu Entri</th>
                              <th class="text-center text-middle" width="50">St. Verif</th>
                              <th class="text-center text-middle" width="100">Waktu Verif</th>
                            </tr>
                          </thead>
                          <?php if (@$main == null) : ?>
                            <tbody>
                              <tr>
                                <td class="text-center" colspan="99"><i>Tidak ada data!</i></td>
                              </tr>
                            </tbody>
                          <?php else : ?>
                            <tbody>
                              <form id="form-multiple" action="" method="post">
                                <?php $i = 1;
                                foreach ($main as $r) : ?>
                                  <tr>
                                    <td class="text-center text-middle"><?= $cookie['cur_page'] + ($i++) ?></td>
                                    <td class="text-middle"><?= $r['user_fullname'] ?></td>
                                    <td class="text-center text-middle"><?= month($r['bulan']) ?> - <?= $r['tahun'] ?></td>
                                    <td class="text-center text-middle"><?= $r['jml_hari_lembur'] ?> Hari</td>
                                    <td class="text-center text-middle"><?= $r['jml_jam_lembur'] ?> Jam</td>
                                    <td class="text-center text-middle"><?= to_date($r['waktu_entri'], '', 'full_date') ?></td>
                                    <td class="text-center text-middle">
                                      <?php if ($r['status_verifikasi'] == 1) : ?>
                                        <i class="fas fa-check-circle text-success"></i>
                                      <?php else : ?>
                                        <i class="fas fa-times-circle text-danger"></i>
                                      <?php endif; ?>
                                    </td>
                                    <td class="text-center text-middle"><?= to_date($r['waktu_verifikasi'], '', 'full_date') ?></td>
                                  </tr>
                                <?php endforeach; ?>
                              </form>
                            </tbody>
                          <?php endif; ?>
                        </table>
                      </div>
                    </div>
                  </div>
                <?php endif; ?>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </div><!-- /.content -->
</div>
<!-- /.content-wrapper -->