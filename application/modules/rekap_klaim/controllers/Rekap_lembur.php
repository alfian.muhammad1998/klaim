<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Rekap_lembur extends MY_Controller
{

  var $menu_id, $menu, $cookie;

  function __construct()
  {
    parent::__construct();

    $this->load->model(array(
      'config/m_config',
      'm_rekap_lembur'
    ));

    $this->menu_id = '05';
    $this->menu = $this->m_config->get_menu($this->menu_id);
    if ($this->menu == null) redirect(site_url() . '/error/error_403');

    //cookie 
    $this->cookie = get_cookie_menu($this->menu_id);
    if (@$this->session->userdata('role_id') == '760fdaab-3074-42dc-9182-19cedb027fdf') {
      $user_id = @$this->session->userdata('user_id');
    } else {
      $user_id = '';
    }
    if ($this->cookie['search'] == null) $this->cookie['search'] = array('bulan' => '', 'tahun' => '', 'user_id' => @$user_id);
    if ($this->cookie['order'] == null) $this->cookie['order'] = array('field' => '', 'type' => '');
    if ($this->cookie['per_page'] == null) $this->cookie['per_page'] = 10;
    if ($this->cookie['cur_page'] == null) 0;
  }

  public function index()
  {
    authorize($this->menu, '_read');
    //cookie
    $this->cookie['cur_page'] = $this->uri->segment(3, 0);
    $this->cookie['total_rows'] = $this->m_rekap_lembur->all_rows($this->cookie);
    set_cookie_menu($this->menu_id, $this->cookie);
    //main data
    $data['menu'] = $this->menu;
    $data['cookie'] = $this->cookie;
    if (@$data['cookie']['search']['bulan'] != '') {
      $data['main'] = $this->m_rekap_lembur->list_data($this->cookie);
    } else {
      $data['main'] = array();
    }
    $data['pagination_info'] = pagination_info(count($data['main']), $this->cookie);
    $data['list_pegawai'] = $this->m_rekap_lembur->list_pegawai();
    $data['list_bulan'] = list_bulan();
    $data['list_tahun'] = list_tahun();
    //set pagination
    set_pagination($this->menu, $this->cookie);
    //render
    $this->render('index', $data);
  }

  function cetak_excel($bulan = null, $tahun = null, $user_id = null)
  {
    ini_set('memory_limit', '-1');
    // Header
    $main = $this->m_rekap_lembur->list_cetak_data($bulan, $tahun, $user_id);
    //
    //Configuration -------------------------------------------------------------------------
    $this->load->file(APPPATH . 'libraries/PHPExcel.php');
    $master_cetak = BASEPATH . 'master_cetak/lembur.xlsx';
    $PHPExcel = PHPExcel_IOFactory::load($master_cetak);

    // sheet 1
    $PHPExcel->setActiveSheetIndex(0);

    //align center
    $align_center = array(
      'alignment' => array(
        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
      )
    );
    //align left
    $align_left = array(
      'alignment' => array(
        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
      )
    );
    //align right
    $align_right = array(
      'alignment' => array(
        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
      )
    );
    //font bold
    $font_bold = array(
      'font' => array(
        'bold'  => 'bold'
      )
    );
    //font underline
    $font_underline = array(
      'font' => array(
        'underline'  => 'underline'
      )
    );
    //font size 12
    $font_size_12 = array(
      'font' => array(
        'size'  => 12
      )
    );
    //wrap text
    $wrap_text = array(
      'alignment' => array(
        'wrap' => 'wrap',
      )
    );
    //create border
    $styleArray = array(
      'borders' => array(
        'allborders' => array(
          'style' => PHPExcel_Style_Border::BORDER_THIN,
          'color' => array('rgb' => '111111'),
        ),
      ),
      'font' => array(
        'size'  => 11,
        'name'  => 'Times New Roman'
      ),
    );
    //Header
    $PHPExcel->getActiveSheet()->setCellValue("D4", ': ' . month($this->cookie['search']['bulan']));
    $PHPExcel->getActiveSheet()->setCellValue("D5", ': ' . $this->cookie['search']['tahun']);
    if (@$this->cookie['search']['user_id'] != '') {
      $get_user = $this->m_rekap_lembur->get_user(@$this->cookie['search']['user_id']);
      $PHPExcel->getActiveSheet()->setCellValue("D6", ': ' . @$get_user['user_fullname']);
    } else {
      $PHPExcel->getActiveSheet()->setCellValue("D6", ': Semua Pegawai');
    }

    // Body
    $i = 10;
    $no = 1;
    foreach ($main as $row) {
      if ($row['status_verifikasi'] == 1) {
        $status_verifikasi = "SUDAH";
      } else {
        $status_verifikasi = "BELUM";
      }
      $PHPExcel->getActiveSheet()->setCellValue("A$i", $no++);
      $PHPExcel->getActiveSheet()->setCellValue("B$i", $row['user_fullname']);
      $PHPExcel->getActiveSheet()->setCellValue("F$i", month($row['bulan']) . ' - ' . $row['tahun']);
      $PHPExcel->getActiveSheet()->setCellValue("G$i", $row['jml_hari_lembur'] . ' Hari');
      $PHPExcel->getActiveSheet()->setCellValue("H$i", $row['jml_jam_lembur'] . ' Jam');
      $PHPExcel->getActiveSheet()->setCellValue("I$i", to_date_indo($row['waktu_entri']));
      $PHPExcel->getActiveSheet()->setCellValue("J$i", @$status_verifikasi);
      $PHPExcel->getActiveSheet()->setCellValue("K$i", to_date_indo($row['waktu_verifikasi']));
      //
      $i++;
    }

    // $PHPExcel->getActiveSheet()->setCellValue("A$i", "Total Akhir");
    // $PHPExcel->getActiveSheet()->getStyle("A$i")->applyFromArray($font_bold);
    // $PHPExcel->getActiveSheet()->mergeCells("A$i:B$i");

    // $PHPExcel->getActiveSheet()->setCellValue("C$i", $tot_akhir_total);
    // $PHPExcel->getActiveSheet()->getStyle("C$i")->applyFromArray($font_bold);

    // Creating Border
    $last_cell = "K" . ($i - 1);
    $PHPExcel->getActiveSheet()->getStyle("A8:$last_cell")->applyFromArray($styleArray);

    // Save it as file ------------------------------------------------------------------
    $file_export = 'rekap-lembur-pegawai-' . strtolower(month(@$this->cookie['search']['bulan'])) . '-' . @$this->cookie['search']['tahun'];
    //
    ob_end_clean();
    header('Content-Type: application/vnd.ms-excel2007');
    header('Content-Disposition: attachment; filename="' . $file_export . '.xlsx"');
    $objWriter = PHPExcel_IOFactory::createWriter($PHPExcel, 'Excel2007');
    $objWriter->save('php://output');
    //-----------------------------------------------------------------------------------
  }
}
